package main

import (
	"context"
	"net/rpc"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/sirupsen/logrus"
	"gitlab.com/jakinss321/microservice_exchange/configs"
	"gitlab.com/jakinss321/microservice_exchange/grpc/exchange"
	"gitlab.com/jakinss321/microservice_exchange/internal/handler"
	"gitlab.com/jakinss321/microservice_exchange/internal/infrastructure/rabbit"
	"gitlab.com/jakinss321/microservice_exchange/internal/infrastructure/rpcclient"
	"gitlab.com/jakinss321/microservice_exchange/internal/infrastructure/rpcserver"
	"gitlab.com/jakinss321/microservice_exchange/internal/middleware"
	"gitlab.com/jakinss321/microservice_exchange/internal/repository"
	"gitlab.com/jakinss321/microservice_exchange/internal/service"
	"gitlab.com/jakinss321/microservice_exchange/internal/worker"
	jsonrpc "gitlab.com/jakinss321/microservice_exchange/json_rpc"
	"gitlab.com/jakinss321/microservice_exchange/server"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

// @title           Documentation of your project API.
// @version         1.0

// @contact.name   api-getway

// @host      localhost:8083
// @securityDefinitions:
//
//	bearerAuth:
//	  type: apiKey
//	  name: Authorization
//	  in: header
//	  description: Bearer token obtained from the authentication server.
//
// @in header
// @name Authorization
func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))
	cfg := configs.InitConfig()
	var conn rpcclient.RPCClient
	var err error

	db, err := repository.NewPostgresDB(cfg)
	if err != nil {
		logrus.Fatalf("failed to initialize db: %s", err.Error())
	}

	rabbit, err := rabbit.NewRabbit(cfg)
	if err != nil {
		logrus.Fatalf("failed to initialize rabbit: %s", err.Error())
	}
	repos := repository.NewRepository(db)
	services := service.NewService(repos, cfg, rabbit)
	if cfg.IfGrpc == "grpc" {
		conn, err = rpcclient.NewGRPC(cfg)
		s := grpc.NewServer()
		exchangeGRPCService := exchange.NewServiceGRPC(services)
		exchange.RegisterExchangeServiceServer(s, exchangeGRPCService)

		grpcServer := rpcserver.NewGRPCServer(cfg, s)
		go func() {
			err := grpcServer.GRPCServe(context.Background())
			if err != nil {
				logrus.Fatal("app: server error", zap.Error(err))
			}
		}()

	} else {
		exchangeRPC := jsonrpc.NewExchangeServiceJSONRPC(services)
		conn, err = rpcclient.NewRpc(cfg)
		jsonRPCServer := rpc.NewServer()
		err = jsonRPCServer.Register(exchangeRPC)
		if err != nil {
			logrus.Fatal("app: server error", zap.Error(err))
		}

		jsonRPC := rpcserver.NewJSONRPC(cfg, jsonRPCServer)
		go func() {
			err := jsonRPC.Serve(context.Background())
			if err != nil {
				logrus.Fatal("app: server error", zap.Error(err))
			}
		}()
	}
	authMiddleware := middleware.AuthsMiddleware(conn)
	worker := worker.NewWorker(services)
	handlers := handler.NewHandler(services, authMiddleware)

	go worker.StartWorker()

	srv := new(server.Server)
	go func() {
		if err := srv.Run(cfg.PortExchange, handlers.InitRoutes()); err != nil {
			logrus.Fatalf("error occured while running http server: %s", err.Error())
		}
	}()
	logrus.Print("Exchange Started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Print("Exchange Shutting Down")

	if err := srv.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}
	if err := db.Close(); err != nil {
		logrus.Errorf("error occured on db connection close: %s", err.Error())
	}

}
