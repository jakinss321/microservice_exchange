package exchange

import (
	context "context"

	"gitlab.com/jakinss321/microservice_exchange/internal/service"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type serverGRPC struct {
	service *service.Service
}

func NewServiceGRPC(service *service.Service) *serverGRPC {
	return &serverGRPC{
		service: service,
	}
}

func (s *serverGRPC) GetHistory(ctx context.Context, req *GetHistoryRequest) (*PriceHistories, error) {
	histories := s.service.GetHistory()
	var protoHistories []*PriceHistory
	for _, history := range *histories {
		protoHistory := &PriceHistory{
			Pair:     history.Pair,
			High:     float32(history.High),
			Low:      float32(history.Low),
			DateTime: timestamppb.New(history.DateTime),
		}
		protoHistories = append(protoHistories, protoHistory)
	}

	return &PriceHistories{Histories: protoHistories}, nil
}

func (s *serverGRPC) FetchTickerData(ctx context.Context, req *FetchTickerDataRequest) (*TickerResponse, error) {
	response, err := s.service.FetchTickerData()
	if err != nil {
		return nil, err
	}

	tickerDataMap := make(map[string]*TickerData)
	for pair, data := range *response {
		tickerData := &TickerData{
			BuyPrice:  float32(data.BuyPrice),
			SellPrice: float32(data.SellPrice),
			LastTrade: float32(data.LastTrade),
			High:      float32(data.High),
			Low:       float32(data.Low),
			Avg:       float32(data.Avg),
			Vol:       float32(data.Vol),
			VolCurr:   float32(data.VolCurr),
			Updated:   data.Updated,
		}
		tickerDataMap[pair] = tickerData
	}

	return &TickerResponse{Data: tickerDataMap}, nil
}

func (s *serverGRPC) GetMaxPrice(context.Context, *GetMaxPriceRequest) (*MaxPrices, error) {
	response := s.service.GetMaxPrice()

	var protoMaxPrices []*MaxPrice
	for _, maxPrice := range *response {
		protoMaxPrice := &MaxPrice{
			Pair:  maxPrice.Pair,
			Price: float32(maxPrice.Price),
		}
		protoMaxPrices = append(protoMaxPrices, protoMaxPrice)
	}

	return &MaxPrices{Prices: protoMaxPrices}, nil

}
func (s *serverGRPC) GetMinPrice(context.Context, *GetMinPriceRequest) (*MinPrices, error) {
	response := s.service.GetMinPrice()

	var protoMinPrices []*MinPrice
	for _, minPrice := range *response {
		protoMinPrice := &MinPrice{
			Pair:  minPrice.Pair,
			Price: float32(minPrice.Price),
		}
		protoMinPrices = append(protoMinPrices, protoMinPrice)
	}

	return &MinPrices{Prices: protoMinPrices}, nil
}
func (s *serverGRPC) GetAveragePrice(context.Context, *GetAveragePriceRequest) (*AveragePrices, error) {
	response := s.service.GetAveragePrice()

	var protoAveragePrices []*AveragePrice

	for _, ap := range *response {
		protoAveragePrice := &AveragePrice{
			Pair:         ap.Pair,
			AveragePrice: float32(ap.AveragePrice),
		}
		protoAveragePrices = append(protoAveragePrices, protoAveragePrice)
	}
	return &AveragePrices{Prices: protoAveragePrices}, nil
}
func (s *serverGRPC) mustEmbedUnimplementedExchangeServiceServer() {}
