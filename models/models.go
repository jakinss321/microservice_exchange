package models

import "time"

type TickerData struct {
	BuyPrice  float64 `json:"buy_price,string"`
	SellPrice float64 `json:"sell_price,string"`
	LastTrade float64 `json:"last_trade,string"`
	High      float64 `json:"high,string"`
	Low       float64 `json:"low,string"`
	Avg       float64 `json:"avg,string"`
	Vol       float64 `json:"vol,string"`
	VolCurr   float64 `json:"vol_curr,string"`
	Updated   int64   `json:"updated"`
}

type PriceHistory struct {
	ID       int64   `gorm:"primary_key;auto_increment"`
	Pair     string  `gorm:"not null"`
	High     float64 `gorm:"not null"`
	Low      float64 `gorm:"not null"`
	DateTime time.Time
}

type TickerResponse map[string]TickerData

type AveragePrices []struct {
	Pair         string  `json:"pair"`
	AveragePrice float64 `json:"average_price"`
}

type MinPrices []struct {
	Pair  string  `json:"pair"`
	Price float64 `json:"price"`
}

type MaxPrices []struct {
	Pair  string  `json:"pair"`
	Price float64 `json:"price"`
}

type PriceHistories []struct {
	Pair     string    `json:"pair"`
	High     float64   `json:"high"`
	Low      float64   `json:"low"`
	DateTime time.Time `json:"date_time"`
}

type Configs struct {
	PortExchange string
	ApiTicker    string
	HostDB       string
	UserDB       string
	NameDB       string
	SSLMode      string
	PasswordDB   string
	PortDB       string
	IfGrpc       string
	PortRPC      string
}
