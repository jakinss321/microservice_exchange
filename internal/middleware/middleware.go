package middleware

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/jakinss321/microservice_exchange/internal/infrastructure/rpcclient"
)

type AuthMiddleware struct {
	rpc rpcclient.RPCClient
}

func AuthsMiddleware(rpc rpcclient.RPCClient) *AuthMiddleware {
	return &AuthMiddleware{rpc: rpc}
}

func (h *AuthMiddleware) AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authToken := c.GetHeader("Authorization")

		if authToken == "" {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Need a token"})
			return
		}

		tokenParts := strings.Split(authToken, " ")
		if len(tokenParts) < 2 || tokenParts[0] != "Bearer" {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Token no Bearer or empty token"})
			return
		}

		userToken, err := h.rpc.CheckToken(tokenParts[1])

		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
			return
		}

		c.Set("user", userToken)

		c.Next()
	}
}
