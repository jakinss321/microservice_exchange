package service

import (
	"gitlab.com/jakinss321/microservice_exchange/internal/infrastructure/rabbit"
	"gitlab.com/jakinss321/microservice_exchange/internal/repository"
	"gitlab.com/jakinss321/microservice_exchange/models"
)

type Exchanges interface {
	GetHistory() *models.PriceHistories
	FetchTickerData() (*models.TickerResponse, error)
	GetMaxPrice() *models.MaxPrices
	GetMinPrice() *models.MinPrices
	GetAveragePrice() *models.AveragePrices
	FetchTicker() error
}

type Service struct {
	Exchanges
	cfg *models.Configs
}

func NewService(repos *repository.Repository, cfg *models.Configs, rabbit *rabbit.Rabbit) *Service {
	return &Service{
		Exchanges: NewExchangeService(repos.Exchanges, cfg, rabbit),
	}
}
