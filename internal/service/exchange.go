package service

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/jakinss321/microservice_exchange/internal/infrastructure/rabbit"
	"gitlab.com/jakinss321/microservice_exchange/internal/repository"
	"gitlab.com/jakinss321/microservice_exchange/models"
)

type ExchangeService struct {
	repo   repository.Exchanges
	cfg    *models.Configs
	rabbit *rabbit.Rabbit
}

func NewExchangeService(repo repository.Exchanges, cfg *models.Configs, rabbit *rabbit.Rabbit) *ExchangeService {
	return &ExchangeService{
		repo:   repo,
		cfg:    cfg,
		rabbit: rabbit,
	}
}

func (e *ExchangeService) GetHistory() *models.PriceHistories {
	return e.repo.GetHistory()
}

func (e *ExchangeService) FetchTicker() error {
	res, err := e.FetchTickerData()
	if err != nil {
		return err
	}

	go e.CheckMinMaxPrice(res)

	err = e.repo.SaveTickerDataToDB(*res)
	if err != nil {
		logrus.Println("Ошибка при сохранении данных в базу данных:", err)
	}
	return nil
}

func (e *ExchangeService) FetchTickerData() (*models.TickerResponse, error) {
	resp, err := http.Get(e.cfg.ApiTicker)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var tickerResponse models.TickerResponse
	err = json.NewDecoder(resp.Body).Decode(&tickerResponse)
	if err != nil {
		return nil, err
	}
	return &tickerResponse, nil
}

func (e *ExchangeService) GetMaxPrice() *models.MaxPrices {
	return e.repo.GetMaxPrice()
}

func (e *ExchangeService) GetMinPrice() *models.MinPrices {
	return e.repo.GetMinPrice()
}

func (e *ExchangeService) GetAveragePrice() *models.AveragePrices {
	return e.repo.GetAveragePrice()
}

func (e *ExchangeService) CheckMinMaxPrice(current *models.TickerResponse) {
	min := e.GetMinPrice()
	max := e.GetMaxPrice()

	for pair, tickerData := range *current {
		var minPrice float64
		for _, p := range *min {
			if p.Pair == pair {
				minPrice = p.Price
				break
			}
		}

		var maxPrice float64
		for _, p := range *max {
			if p.Pair == pair {
				maxPrice = p.Price
				break
			}
		}

		if tickerData.LastTrade <= minPrice-100 {
			e.rabbit.SendMessage(fmt.Sprintf("%s:%f", pair, tickerData.LastTrade))
		}
		if tickerData.LastTrade >= maxPrice+100 {
			e.rabbit.SendMessage(fmt.Sprintf("%s:%f", pair, tickerData.LastTrade))
		}
	}
}
