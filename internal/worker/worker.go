package worker

import (
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/jakinss321/microservice_exchange/internal/service"
)

type Workerer struct {
	service *service.Service
}

func NewWorker(service *service.Service) *Workerer {
	return &Workerer{service: service}
}

func (w *Workerer) StartWorker() {
	ticker := time.NewTicker(10 * time.Second)
	for range ticker.C {
		err := w.service.FetchTicker()
		if err != nil {

			logrus.Println("Ошибка при получении данных:", err)
			continue
		}
	}
}
