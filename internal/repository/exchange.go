package repository

import (
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/jakinss321/microservice_exchange/models"
)

type ExchangePostgres struct {
	db *gorm.DB
}

func NewExchangePostgres(db *gorm.DB) *ExchangePostgres {
	return &ExchangePostgres{db: db}
}

func (e *ExchangePostgres) GetHistory() *models.PriceHistories {
	PriceHistories := &models.PriceHistories{}
	e.db.Table("price_histories").Select("pair, high, low, date_time").Order("pair, date_time").Scan(&PriceHistories)
	return PriceHistories
}

func (e *ExchangePostgres) GetMaxPrice() *models.MaxPrices {
	maxPrices := &models.MaxPrices{}
	e.db.Table("price_histories").Select("pair, MAX(high) as price").Group("pair").Scan(&maxPrices)
	return maxPrices
}

func (e *ExchangePostgres) GetMinPrice() *models.MinPrices {
	minPrices := &models.MinPrices{}
	e.db.Table("price_histories").Select("pair, MIN(low) as price").Group("pair").Scan(&minPrices)
	return minPrices
}

func (e *ExchangePostgres) GetAveragePrice() *models.AveragePrices {
	averagePrices := &models.AveragePrices{}
	e.db.Table("price_histories").
		Select("pair, AVG((high + low) / 2) as average_price").
		Group("pair").
		Scan(averagePrices)
	return averagePrices
}

func (e *ExchangePostgres) SaveTickerDataToDB(tickerData models.TickerResponse) error {
	for pair, data := range tickerData {
		priceHistory := models.PriceHistory{
			Pair:     pair,
			High:     data.High,
			Low:      data.Low,
			DateTime: time.Now(),
		}
		e.db.Save(&priceHistory)
	}
	return nil
}
