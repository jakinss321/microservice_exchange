package repository

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/jakinss321/microservice_exchange/models"
)

type Exchanges interface {
	GetHistory() *models.PriceHistories
	GetMaxPrice() *models.MaxPrices
	GetMinPrice() *models.MinPrices
	GetAveragePrice() *models.AveragePrices
	SaveTickerDataToDB(tickerData models.TickerResponse) error
}

type Repository struct {
	Exchanges
}

func NewRepository(db *gorm.DB) *Repository {
	return &Repository{
		Exchanges: NewExchangePostgres(db),
	}
}
