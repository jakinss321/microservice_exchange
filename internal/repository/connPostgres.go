package repository

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/jakinss321/microservice_exchange/models"
)

func NewPostgresDB(cfg *models.Configs) (*gorm.DB, error) {
	db, err := gorm.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s password=%s", cfg.HostDB, cfg.PortDB, cfg.UserDB, cfg.NameDB, cfg.SSLMode, cfg.PasswordDB))
	if err != nil {
		return nil, err
	}

	db.AutoMigrate(&models.PriceHistory{})

	return db, nil
}
