package handler

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/jakinss321/microservice_exchange/docs"
	"gitlab.com/jakinss321/microservice_exchange/internal/middleware"
	"gitlab.com/jakinss321/microservice_exchange/internal/service"
)

type Handler struct {
	services       *service.Service
	authMiddleware *middleware.AuthMiddleware
}

func NewHandler(service *service.Service, authMiddleware *middleware.AuthMiddleware) *Handler {
	return &Handler{
		services:       service,
		authMiddleware: authMiddleware,
	}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.Default()
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	exchangeGroup := router.Group("/exchange", h.authMiddleware.AuthMiddleware())
	{
		exchangeGroup.GET("/ticker", h.getTickerHandler)
		exchangeGroup.GET("/history", h.getHistoryHandler)
		exchangeGroup.GET("/max-price", h.getMaxPriceHandler)
		exchangeGroup.GET("/min-price", h.getMinPriceHandler)
		exchangeGroup.GET("/average-price", h.getAveragePriceHandler)
	}
	return router
}
