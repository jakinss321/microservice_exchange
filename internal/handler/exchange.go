package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetHistory возвращает историю цен.
// @Summary Возвращает историю цен.
// @Security ApiKeyAuth
// @Description Возвращает историю цен.
// @Tags Price
// @Produce json
// @Success 200 {object} models.PriceHistories
// @Router /exchange/history [get]
func (h *Handler) getHistoryHandler(c *gin.Context) {
	c.JSON(http.StatusOK, h.services.GetHistory())
}

// GetMaxPrice возвращает максимальную цену.
// @Summary Возвращает максимальную цену.
// @Security ApiKeyAuth
// @Description Возвращает максимальную цену.
// @Tags Price
// @Produce json
// @Success 200 {object} models.MaxPrices
// @Router /exchange/max-price [get]
func (h *Handler) getMaxPriceHandler(c *gin.Context) {
	c.JSON(http.StatusOK, h.services.GetMaxPrice())
}

// GetMinPrice возвращает минимальную цену.
// @Summary Возвращает минимальную цену.
// @Security ApiKeyAuth
// @Description Возвращает минимальную цену.
// @Tags Price
// @Produce json
// @Success 200 {object} models.MinPrices
// @Router /exchange/min-price [get]
func (h *Handler) getMinPriceHandler(c *gin.Context) {
	c.JSON(http.StatusOK, h.services.GetMinPrice())
}

// GetAveragePrice возвращает среднюю цену.
// @Summary Возвращает среднюю цену.
// @Security ApiKeyAuth
// @Description Возвращает среднюю цену.
// @Tags Price
// @Produce json
// @Success 200 {object} models.AveragePrices
// @Router /exchange/average-price [get]
func (h *Handler) getAveragePriceHandler(c *gin.Context) {
	c.JSON(http.StatusOK, h.services.GetAveragePrice())
}

// @Summary Возвращает данные тикера.
// @Security ApiKeyAuth
// @Description Возвращает данные тикера.
// @Tags Ticker
// @Produce json
// @Success 200 {object} models.TickerResponse
// @Router /exchange/ticker [get]
func (h *Handler) getTickerHandler(c *gin.Context) {
	tickerData, err := h.services.FetchTickerData()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Ошибка при получении данных тикера"})
		return
	}
	c.JSON(http.StatusOK, tickerData)
}
