package rpcclient

import (
	"errors"
	"net/rpc"
	"net/rpc/jsonrpc"

	"github.com/sirupsen/logrus"
	"gitlab.com/jakinss321/microservice_exchange/models"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/tools/cryptography"
)

type RPCClient interface {
	CheckToken(token string) (*cryptography.UserClaims, error)
}

type Rpc struct {
	cfg           *models.Configs
	userClientRpc *rpc.Client
}

func NewRpc(cfg *models.Configs) (*Rpc, error) {
	userClient, err := jsonrpc.Dial("tcp", "userapi:3455")
	if err != nil {
		return nil, err
	}
	logrus.Printf("Connected to JSON-RPC service at userapi:3455")
	return &Rpc{
		cfg:           cfg,
		userClientRpc: userClient,
	}, nil
}

func (r *Rpc) CheckToken(token string) (*cryptography.UserClaims, error) {
	out := &cryptography.UserClaims{}

	err := r.userClientRpc.Call("UserServiceJSONRPC.ParseToken", token, &out)
	if err != nil {
		return out, err
	}

	if out.ID == "" {
		return out, errors.New("token exp")
	}
	return out, nil
}

// func (h *Rpc) CheckToken(in string) (*cryptography.UserClaims, error) {

// 	out := &cryptography.UserClaims{}

// 	err := h.userClientRpc.Call("UserServiceJSONRPC.ParseToken", in, &out)
// 	if err != nil {
// 		return out, err
// 	}

// 	if out.ID == "" {
// 		return out, errors.New("token exp")
// 	}
// 	return out, nil
// }
