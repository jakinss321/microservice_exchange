package rpcclient

import (
	"context"

	"github.com/sirupsen/logrus"
	"gitlab.com/jakinss321/microservice_exchange/models"
	servicepb "gitlab.com/jakinss321/microservice_user/grpc/user"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/tools/cryptography"
	"google.golang.org/grpc"
)

type Grpc struct {
	cfg                   *models.Configs
	userServiceClientGRPC servicepb.UserServiceClient
}

func NewGRPC(cfg *models.Configs) (*Grpc, error) {
	userClient, err := grpc.Dial("userapi:3455", grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	logrus.Printf("Connected to GRPC service at userapi:3455")
	userServiceClient := servicepb.NewUserServiceClient(userClient)

	return &Grpc{
		cfg:                   cfg,
		userServiceClientGRPC: userServiceClient,
	}, nil
}

func (r *Grpc) CheckToken(token string) (*cryptography.UserClaims, error) {
	in := &servicepb.TokenRequest{
		Token: token,
	}
	res, err := r.userServiceClientGRPC.ParseToken(context.Background(), in)
	if err != nil {
		return nil, err
	}
	out := &cryptography.UserClaims{
		ID:     res.Id,
		Role:   res.Role,
		Groups: res.Groups,
		Layers: res.Layers,
	}

	return out, nil
}
