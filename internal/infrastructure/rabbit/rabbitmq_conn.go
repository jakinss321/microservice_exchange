package rabbit

import (
	"log"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/jakinss321/microservice_exchange/models"
)

type Rabbit struct {
	ch  *amqp.Channel
	cfg *models.Configs
}

func NewRabbit(cfg *models.Configs) (*Rabbit, error) {
	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		logrus.Printf("Failed to connect to RabbitMQ: %v", err)
		return nil, err
	}

	ch, err := conn.Channel()
	if err != nil {
		logrus.Fatalf("Failed to open a channel: %v", err)
	}

	return &Rabbit{
		ch:  ch,
		cfg: cfg,
	}, nil
}

func (r *Rabbit) SendMessage(message string) {
	queue := "telegram-messages"

	err := r.ch.Publish(
		"",
		queue,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(message),
		},
	)
	if err != nil {
		log.Fatalf("Failed to publish a message: %v", err)
	}

	log.Printf("Message sent to queue '%s'\n", queue)
}
