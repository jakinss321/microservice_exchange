package rpcserver

import (
	"context"
	"fmt"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"

	"github.com/sirupsen/logrus"
	"gitlab.com/jakinss321/microservice_exchange/models"
	"go.uber.org/zap"
)

type ServerJSONRPC struct {
	cfg *models.Configs
	srv *rpc.Server
}

func NewJSONRPC(conf *models.Configs, srv *rpc.Server) *ServerJSONRPC {
	return &ServerJSONRPC{
		cfg: conf,
		srv: srv,
	}
}

func (s *ServerJSONRPC) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var l net.Listener
		l, err = net.Listen("tcp", fmt.Sprintf(":%s", s.cfg.PortRPC))
		if err != nil {
			logrus.Error("json rpc server register error", zap.Error(err))
			chErr <- err
		}

		logrus.Info("json rpc server started", zap.String("port", s.cfg.PortRPC))
		var conn net.Conn
		for {
			select {
			case <-ctx.Done():
				logrus.Error("json rpc: stopping server")
				return
			default:
				var connErr error
				conn, connErr = l.Accept()
				if err != nil {
					logrus.Error("json rpc: net tcp accept", zap.Error(connErr))
				}
				go s.srv.ServeCodec(jsonrpc.NewServerCodec(conn))
			}
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}

	return err
}
