package rpcserver

import (
	"context"
	"fmt"
	"net"

	"github.com/sirupsen/logrus"
	"gitlab.com/jakinss321/microservice_exchange/models"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

type ServerGRPC struct {
	cfg *models.Configs
	srv *grpc.Server
}

func NewGRPCServer(cfg *models.Configs, srv *grpc.Server) *ServerGRPC {
	return &ServerGRPC{
		cfg: cfg,
		srv: srv,
	}
}

func (s *ServerGRPC) GRPCServe(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		l, err := net.Listen("tcp", fmt.Sprintf(":%s", s.cfg.PortRPC))
		if err != nil {
			logrus.Error("grpc server register error", zap.Error(err))
			chErr <- err
		}

		logrus.Info("grpc server started", zap.String("port", s.cfg.PortRPC))
		err = s.srv.Serve(l)
		if err != nil {
			logrus.Error("grpc server error", zap.Error(err))
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}

	return err
}
