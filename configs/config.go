package configs

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/jakinss321/microservice_exchange/models"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}
}

func InitConfig() *models.Configs {
	return &models.Configs{
		PortExchange: os.Getenv("PORT"),
		ApiTicker:    os.Getenv("API_TICKER"),
		HostDB:       os.Getenv("HOSTDB"),
		UserDB:       os.Getenv("USERDB"),
		NameDB:       os.Getenv("NAMEDB"),
		SSLMode:      os.Getenv("SSLMODEDB"),
		PasswordDB:   os.Getenv("PASSWORDDB"),
		PortDB:       os.Getenv("PORTDB"),
		IfGrpc:       os.Getenv("IFGRPC"),
		PortRPC:      os.Getenv("PORTRPC"),
	}

}
