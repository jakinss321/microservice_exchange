package jsonrpc

import (
	"gitlab.com/jakinss321/microservice_exchange/internal/service"
	"gitlab.com/jakinss321/microservice_exchange/models"
)

type ExchangeServiceJSONRPC struct {
	service *service.Service
}

func NewExchangeServiceJSONRPC(service *service.Service) *ExchangeServiceJSONRPC {
	return &ExchangeServiceJSONRPC{
		service: service,
	}
}

func (t *ExchangeServiceJSONRPC) GetHistory(out *models.PriceHistories) error {
	*out = *t.service.GetHistory()
	return nil
}
func (t *ExchangeServiceJSONRPC) FetchTickerData(out *models.TickerResponse) (err error) {
	tickerResponse, err := t.service.FetchTickerData()
	if err != nil {
		return err
	}
	*out = *tickerResponse
	return nil
}
func (t *ExchangeServiceJSONRPC) GetMaxPrice(out *models.MaxPrices) error {
	*out = *t.service.GetMaxPrice()
	return nil
}
func (t *ExchangeServiceJSONRPC) GetMinPrice(out *models.MinPrices) error {
	*out = *t.service.GetMinPrice()
	return nil
}
func (t *ExchangeServiceJSONRPC) GetAveragePrice(out *models.AveragePrices) error {
	*out = *t.service.GetAveragePrice()
	return nil
}
